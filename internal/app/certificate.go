package app

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"math/big"
	"os"
	"time"
)

// CertInfo contain info about key and cert
type CertInfo struct {
	PrivateKey interface{}
	KeyPEM     []byte
	Cert       *x509.Certificate
	CertPEM    []byte
}

// Generator perform cert generation and store generated certs
type Generator struct {
	root  *CertInfo
	certs map[string]*tls.Certificate
}

// NewCertificateGenerator create new instance of generator
func NewCertificateGenerator() (*Generator, error) {
	g := Generator{
		certs: make(map[string]*tls.Certificate),
	}

	// Try to read root cert from disk
	if info, err := ReadRootCert("rootCert.pem", "rootKey.pem"); err == nil {
		g.root = info
		return &g, nil
	}

	// Generate root cert if doesn't exist
	info, err := CreateRootCert()
	if err != nil {
		return nil, err
	}

	// Save cert pem and key pem
	if err := ioutil.WriteFile("rootCert.pem", info.CertPEM, 0600); err != nil {
		return nil, err
	}
	if err := ioutil.WriteFile("rootKey.pem", info.KeyPEM, 0600); err != nil {
		return nil, err
	}

	g.root = info
	return &g, nil
}

// GetCertPool get cert pool
func (g Generator) GetCertPool() *x509.CertPool {
	certPool := x509.NewCertPool()
	certPool.AppendCertsFromPEM(g.root.KeyPEM)
	return certPool
}

// ReadRootCert get root cert from disk
func ReadRootCert(certFile, keyFile string) (*CertInfo, error) {
	certPEMBlock, err := ioutil.ReadFile(certFile)
	if err != nil {
		return nil, err
	}
	keyPEMBlock, err := ioutil.ReadFile(keyFile)
	if err != nil {
		return nil, err
	}

	cert, err := tls.X509KeyPair(certPEMBlock, keyPEMBlock)
	if err != nil {
		return nil, err
	}

	cert.Leaf, err = x509.ParseCertificate(cert.Certificate[0])
	if err != nil {
		return nil, err
	}

	return &CertInfo{
		Cert:       cert.Leaf,
		CertPEM:    certPEMBlock,
		PrivateKey: cert.PrivateKey,
		KeyPEM:     keyPEMBlock,
	}, nil
}

// CreateRootCert create root cert
func CreateRootCert() (*CertInfo, error) {
	hostname, err := os.Hostname()
	if err != nil {
		return nil, err
	}

	privateKey, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	if err != nil {
		return nil, fmt.Errorf("failed to generate random key: %s", err)
	}

	serial, err := genSerialNumber()
	if err != nil {
		return nil, fmt.Errorf("failed to generate serial number: %s", err)
	}

	tmpl := &x509.Certificate{
		SerialNumber:          serial,
		Subject:               pkix.Name{CommonName: hostname},
		SignatureAlgorithm:    x509.ECDSAWithSHA256,
		NotBefore:             time.Now(),
		NotAfter:              time.Now().Add(5 * 365 * 24 * time.Hour),
		IsCA:                  true,
		BasicConstraintsValid: true,
		MaxPathLen:            2,
		KeyUsage: x509.KeyUsageDigitalSignature |
			x509.KeyUsageContentCommitment |
			x509.KeyUsageKeyEncipherment |
			x509.KeyUsageDataEncipherment |
			x509.KeyUsageKeyAgreement |
			x509.KeyUsageCertSign |
			x509.KeyUsageCRLSign,
	}

	keyDER, err := x509.MarshalECPrivateKey(privateKey)
	if err != nil {
		return nil, fmt.Errorf("failed marshalling EC private key to DER format: %s", err)
	}

	cert, certPEM, err := createCert(tmpl, tmpl, privateKey.Public(), privateKey)
	if err != nil {
		return nil, fmt.Errorf("failed creating certificate: %s", err)
	}

	keyPEM := pem.EncodeToMemory(&pem.Block{
		Type:  "ECDSA PRIVATE KEY",
		Bytes: keyDER,
	})

	return &CertInfo{
		Cert:       cert,
		CertPEM:    certPEM,
		PrivateKey: privateKey,
		KeyPEM:     keyPEM,
	}, nil
}

// genSerialNumber generate serial number
func genSerialNumber() (*big.Int, error) {
	serialNumberLimit := new(big.Int).Lsh(big.NewInt(1), 128)
	serialNumber, err := rand.Int(rand.Reader, serialNumberLimit)
	if err != nil {
		return nil, err
	}

	return serialNumber, nil
}

// Create generate cert for host
func (g *Generator) Create(names []string) (*tls.Certificate, error) {
	if cert, exists := g.certs[names[0]]; exists {
		var err error
		cert.Leaf, err = x509.ParseCertificate(cert.Certificate[0])
		if err == nil {
			if time.Now().Before(cert.Leaf.NotAfter) {
				log.Println("get cert from cache for", names[0])
				return cert, nil
			}
		}
	}

	log.Println("create cert for", names[0])
	clientKey, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	if err != nil {
		return nil, errors.New("failed to generate random key: " + err.Error())
	}

	clientCertTmpl, err := createTemplate()
	if err != nil {
		return nil, err
	}

	clientCertTmpl.ExtKeyUsage = []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth, x509.ExtKeyUsageClientAuth}
	clientCertTmpl.DNSNames = names

	_, clientCertPEM, err := createCert(clientCertTmpl, g.root.Cert, &clientKey.PublicKey, g.root.PrivateKey)
	if err != nil {
		return nil, err
	}

	clientKeyDER, err := x509.MarshalECPrivateKey(clientKey)
	if err != nil {
		return nil, err
	}

	clientKeyPEM := pem.EncodeToMemory(&pem.Block{
		Type: "ECDSA PRIVATE KEY", Bytes: clientKeyDER,
	})
	clientTLSCert, err := tls.X509KeyPair(clientCertPEM, clientKeyPEM)
	if err != nil {
		return nil, errors.New("invalid key pair: " + err.Error())
	}

	g.certs[names[0]] = &clientTLSCert
	return &clientTLSCert, nil
}

// createTemplate create base template
func createTemplate() (*x509.Certificate, error) {
	serialNumber, err := genSerialNumber()
	if err != nil {
		return nil, fmt.Errorf("failed to generate serial number: %s", err)
	}

	tmpl := x509.Certificate{
		SerialNumber:          serialNumber,
		Subject:               pkix.Name{CommonName: "Zorin"},
		SignatureAlgorithm:    x509.ECDSAWithSHA256,
		NotBefore:             time.Now(),
		NotAfter:              time.Now().Add(time.Hour),
		BasicConstraintsValid: true,
		KeyUsage: x509.KeyUsageDigitalSignature |
			x509.KeyUsageContentCommitment |
			x509.KeyUsageKeyEncipherment |
			x509.KeyUsageDataEncipherment |
			x509.KeyUsageKeyAgreement |
			x509.KeyUsageCertSign |
			x509.KeyUsageCRLSign,
	}

	return &tmpl, nil
}

// createCert create x509.Certificate and cert pem
func createCert(template, parent *x509.Certificate, pub interface{},
	parentPriv interface{}) (*x509.Certificate, []byte, error) {
	certDER, err := x509.CreateCertificate(rand.Reader, template, parent, pub, parentPriv)
	if err != nil {
		return nil, []byte{}, err
	}

	cert, err := x509.ParseCertificate(certDER)
	if err != nil {
		return nil, []byte{}, fmt.Errorf("failed parsing certificate from DER format: %s", err)
	}

	b := pem.Block{Type: "CERTIFICATE", Bytes: certDER}
	certPEM := pem.EncodeToMemory(&b)
	return cert, certPEM, nil
}
