package app

import (
	"crypto/tls"
	"io"
	"log"
	"net"
	"net/http"
	"time"
)

// Proxy provide http and htttps proxy
type Proxy struct {
	client  http.Client
	certGen *Generator
}

// NewProxy create new instance of proxy
func NewProxy() (*Proxy, error) {
	certGen, err := NewCertificateGenerator()
	if err != nil {
		return nil, err
	}

	return &Proxy{
		client: http.Client{
			Timeout: 5 * time.Second,
		},
		certGen: certGen,
	}, nil
}

// handleHTTP proxy http request
func (p Proxy) handleHTTP(w http.ResponseWriter, r *http.Request) {
	req, err := http.NewRequest(r.Method, r.RequestURI, r.Body)
	r.Body.Close()
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	resp, err := p.client.Do(req)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	for header, values := range resp.Header {
		for _, value := range values {
			w.Header().Add(header, value)
		}
	}
	_, err = io.Copy(w, resp.Body)
	resp.Body.Close()
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

// handleHTTPS proxy https request
func (p Proxy) handleHTTPS(w http.ResponseWriter, r *http.Request) {
	host, _, err := net.SplitHostPort(r.Host)
	if err != nil {
		log.Fatal("failed spliting host:", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	clientTLSCert, err := p.certGen.Create([]string{host})
	if err != nil {
		log.Fatal("failed generating cert:", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	hijacker, ok := w.(http.Hijacker)
	if !ok {

		log.Fatal("failed converting to http.Hijacker:", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	client, _, err := hijacker.Hijack()
	if err != nil {
		log.Fatal("failed hijacker.Hijack():", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if _, err := client.Write([]byte("HTTP/1.1 200 OK\r\n\r\n")); err != nil {
		log.Fatal("failed writing ok response:", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	var conn *tls.Conn
	clientConn := tls.Server(client, &tls.Config{
		Certificates: []tls.Certificate{*clientTLSCert},
		RootCAs:      p.certGen.GetCertPool(),
		GetCertificate: func(hello *tls.ClientHelloInfo) (*tls.Certificate, error) {
			conn, err = tls.Dial("tcp", r.Host, &tls.Config{
				ServerName: hello.ServerName,
			})
			if err != nil {
				log.Println("failed connect to server:", err)
				return nil, err
			}

			cert, err := p.certGen.Create([]string{hello.ServerName})
			if err != nil {
				log.Fatal("failed GetCertificate:", err)
			}

			return cert, nil
		}})
	defer clientConn.Close()

	if err := clientConn.Handshake(); err != nil {
		log.Println("failed handshake with client:", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if err := conn.Handshake(); err != nil {
		log.Fatal("failed handshake with server", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if _, err := copyBuffer(conn, clientConn); err != nil {
		log.Println("failed writing to server", err)
		return
	}

	if _, err := copyBuffer(clientConn, conn); err != nil {
		log.Println("failed writing to client", err)
		return
	}
}

// copyBuffer copy data from tls.conn to io.Writer
func copyBuffer(dst io.Writer, src *tls.Conn) (written int64, err error) {
	size := 32 * 1024
	buf := make([]byte, size)

	for {
		if err := src.SetReadDeadline(time.Now().Add(time.Second)); err != nil {
			log.Fatal(err)
		}
		nr, er := src.Read(buf)
		if e, ok := er.(net.Error); ok {
			if e.Timeout() {
				return written, nil
			}
		}
		if nr > 0 {
			nw, ew := dst.Write(buf[0:nr])
			if nw > 0 {
				written += int64(nw)
			}
			if ew != nil {
				err = ew
				break
			}
			if nr != nw {
				err = io.ErrShortWrite
				break
			}
		}

		if er != nil {
			if er != io.EOF {
				err = er
			}
			break
		}
	}
	return written, err
}

// ServeHTTP process request
func (p Proxy) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	log.Printf("Receive request. Method: %s. Host: %s", r.Method, r.URL)
	if r.Method == http.MethodConnect {
		p.handleHTTPS(w, r)
		log.Printf("Request processed. Method: %s. Host: %s", r.Method, r.URL)
		return
	}
	log.Printf("Request processed. Method: %s. Host: %s", r.Method, r.URL)
	p.handleHTTP(w, r)
}
