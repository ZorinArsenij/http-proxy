package main

import (
	"log"
	"net/http"

	"gitlab.com/ZorinArsenij/http-proxy/internal/app"
)

func main() {
	proxy, err := app.NewProxy()
	if err != nil {
		log.Fatalf("failed creation of proxy: %s", err)
	}

	log.Println(http.ListenAndServe(":8080", proxy))
}
